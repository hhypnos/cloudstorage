<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Permission;
use App\User;

class AccountController extends Controller
{
    public  function login()
    {
        return view('authentification.login')->with(['title'=>'FutureLaboratory']);
    }

    public  function cloud()
    {
        return view('authentification.cloud')->with(['title'=>'FutureLaboratory']);
    }

    public function scanner()
    {

        $dir = "cloudStorage";

// Run the recursive function
        $response = $this->scan($dir);
        $folder = request('checkFolder') !== null ? request('checkFolder') : explode("/",$response[0]['path'])[count(explode("/",$response[0]['path']))-1];
        if(Permission::where('folder',$folder)->first() || auth()->user()->userLevel > 3) {
            if (Permission::where('userID', auth()->user()->id)->where('folder', $folder)->first() || auth()->user()->userLevel >= Permission::where('folder', $folder)->first()['folderLevel']) {
// Output the directory listing as JSON
                return array(
                    "name" => "cloudStorage",
                    "type" => "folder",
                    "path" => $dir,
                    "items" => $response
                );
            }
        }
    }
// This function scans the files folder recursively, and builds a large array
    private function scan($dir)
    {

        $files = array();

        // Is there actually such a folder/file?

        if (file_exists($dir)) {

            foreach (scandir($dir) as $f) {

                if (!$f || $f[0] == '.') {
                    continue; // Ignore hidden files
                }
                if (is_dir($dir . '/' . $f)) {
                    if (Permission::where('folder', $f)->first() || auth()->user()->userLevel > 3) {
                        if (Permission::where('userID', auth()->user()->id)->where('folder', $f)->first() || auth()->user()->userLevel >= Permission::where('folder', $f)->first()['folderLevel']) {
                            // The path is a folder
                            $files[] = array(
                                "name" => $f,
                                "type" => "folder",
                                "path" => $dir . '/' . $f,
                                "items" => $this->scan($dir . '/' . $f) // Recursively get the contents of the folder
                            );
                        }
                    }
                } else {
                        // It is a file
                        $files[] = array(
                            "name" => $f,
                            "type" => "file",
                            "path" => $dir . '/' . $f,
                            "size" => filesize($dir . '/' . $f) // Gets the size of this file
                        );
                }

            }

            return $files;
        }
    }

    /*
 * AUTHENTIFICATION
 */
    public function signin()
    {
//        Check if session exists (if yes than use it)(90,91 lines)
        if(User::where('email', session('email') ? session('email') : request('email'))->exists()) {
            $user = User::where('email', session('email') ? session('email') : request('email'))->first();
            if ($user && !session('email')) {
                session(['email' => request('email')]);
            }

            if (session('email') && request('password')) {
                if ($user->userLevel != 0) {
                    if (!auth()->attempt(array('email' => session('email'), 'password' => request('password')), false)) {
                        session(['errorType'=>'info']);
                        return back()->withErrors(['info'=>'Incorrect password']);
                    }

                    return redirect('cloud');
                } else {
                    session(['errorType'=>'error']);
                    return back()->withErrors(['info'=>'User Blocked']);
                }
            }
            session(['errorType'=>'success']);
            return back()->withErrors(['info'=>'User '.session('email').' exists']);
        }
        session(['errorType'=>'warning']);
        return back()->withErrors(['info'=>'User Doesn"t exists']);
    }

    public function switchAccount()
    {
            session()->forget('email');
            return redirect(route('login'));
    }

    public function logout()
    {

        auth()->logout();

        return redirect('login');
    }
}