<!DOCTYPE html>
<html lang="en">
<head>
    <title>ChangeLog</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.1.1/js/all.js" integrity="sha384-BtvRZcyfv4r0x/phJt9Y9HhnN5ur1Z+kZbKVgzVBAlQZX4jvAuImlIz+bG7TS00a" crossorigin="anonymous"></script>
    <!-- Global CSS -->
    <link rel="stylesheet" href="{{URL::to('assets_changelog/plugins/bootstrap/css/bootstrap.min.css')}}">
    <!-- Plugins CSS -->
    <link rel="stylesheet" href="{{URL::to('assets_changelog/plugins/prism/prism.css')}}">
    <link rel="stylesheet" href="{{URL::to('assets_changelog/plugins/lightbox/dist/ekko-lightbox.css')}}">
    <link rel="stylesheet" href="{{URL::to('assets_changelog/plugins/elegant_font/css/style.css')}}">

    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="{{URL::to('assets_changelog/css/styles.css')}}">

</head>

<body class="body-purple">
<div class="page-wrapper">
    <!-- ******Header****** -->
    <header id="header" class="header">
        <div class="container">
            <div class="branding">
                <h1 class="logo">
                    <a href="{{URL::to('/')}}">
                        <span aria-hidden="true" class="icon_documents_alt icon" style="color:#40babd"></span>
                        <span class="text-highlight">Cloud</span><span class="text-bold">Storage</span>
                    </a>
                </h1>
            </div><!--//branding-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{URL::to('/')}}">Home</a></li>
                <li class="breadcrumb-item active">Change Log</li>
            </ol>
        </div><!--//container-->
    </header><!--//header-->
    <div class="doc-wrapper">
        <div class="container">
            <div id="doc-header" class="doc-header text-center">
                <h1 class="doc-title"><span aria-hidden="true" class="icon icon_lifesaver"></span> Change Log</h1>
                <div class="meta"><i class="far fa-clock"></i> Last updated: February 17th, 2019</div>
            </div><!--//doc-header-->
            <div class="doc-body row">
                <div class="doc-content col-md-9 col-12 order-1">
                    <div class="content-inner">
                        <section id="general" class="doc-section">
                            <h2 class="section-title">General</h2>

                            <div class="section-block">
                                <h3 class="question"><i class="fas fa-question-circle"></i> ChangeLog</h3>
                                <div class="answer" >Added styled ChangeLog 😂</div>
                            </div><!--//section-block-->

                            <div class="section-block">
                                <h3 class="question"><i class="fas fa-question-circle"></i> Styles</h3>
                                <div class="answer" >Added styles on error messages</div>
                            </div><!--//section-block-->

                            <div class="section-block">
                                <h3 class="question"><i class="fas fa-question-circle"></i> Nginx Bug Fix</h3>
                                <div class="answer" >Bug fix with Nginx Config</div>
                                <ul>
                                    <li>block direct file access (check with reverse proxy if user is authentificated)</li>
                                    <li>Multiple Project support in one config file ("default")</li>
                                </ul>
                            </div><!--//section-block-->

                            <div class="section-block">
                                <h3 class="question"><i class="fas fa-question-circle"></i> Bug Fix</h3>
                                <div class="answer">Bug fix with protected files && CheckPath</div>
                            </div><!--//section-block-->

                            <div class="section-block">
                                <h3 class="question"><i class="fas fa-question-circle"></i> Permissions</h3>
                                <div class="answer">user which level is higher than 3 can see hidden folders
                                    <li>Updated file AccountController <code class="language-php">public function scanner(){...}</code></li>
                                    <pre><code class="language-php">if(Permission::where('folder',$folder)->first() || auth()->user()->userLevel > 3) {...}
                                    </code></pre>
                                </div>
                            </div><!--//section-block-->

                            <div class="section-block">
                                <h3 class="question"><i class="fas fa-question-circle"></i> First Commit</h3>
                                <div class="answer"></div>
                            </div><!--//section-block-->

                        </section><!--//doc-section-->

                        <section id="install" class="doc-section">
                            <h2 class="section-title">Install</h2>
                            <div class="section-block">
                                <h3 class="question"><i class="fas fa-question-circle"></i> Requirement</h3>
                                <div class="answer">
                                    <ul>
                                        <li>Composer</li>
                                        <li>PHP 7.2.* +</li>
                                    </ul>
                                </div><!--//anwser-->

                                <div class="section-block">
                                    <h3 class="question"><i class="fas fa-question-circle"></i> Contribution</h3>
                                    <div class="answer">
                                        <ul>
                                            <li>clone project</li>
                                            <li>Open [ProjectFolder] with terminal and write</li>
                                            <li>Run Command <code class="language-terminal">composer install</code></li>
                                            <li>configure .env file</li>
                                            <li>Create database</li>
                                            <li>Fill required info for DB in .env file</li>
                                            <li>Run Command <code class="language-terminal">php artisan key:generate</code></li>
                                            <li>Open [ProjectFolder] with terminal and write</li>
                                            <li>Run Command
                                                <ul>
                                                    <li><code class="language-terminal">php artisan migrate </code></li>
                                                    <li><code class="language-terminal">composer dump-autoload </code></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div><!--//section-block-->

                            </div><!--//section-block-->
                        </section><!--//doc-section-->
                    </div><!--//content-inner-->
                </div><!--//doc-content-->
                <div class="doc-sidebar col-md-3 col-12 order-0 d-none d-md-flex">
                    <div id="doc-nav" class="doc-nav">
                        <nav id="doc-menu" class="nav doc-menu flex-column sticky">
                            <a class="nav-link scrollto" href="#general">General</a>
                            <a class="nav-link scrollto" href="#install">Install</a>
                            <a class="nav-link" href="https://gitlab.com/hhypnos/cloudstorage" target="_blank">GitLab</a>
                        </nav><!--//doc-menu-->
                    </div>
                </div><!--//doc-sidebar-->
            </div><!--//doc-body-->
        </div><!--//container-->
    </div><!--//doc-wrapper-->
</div><!--//page-wrapper-->

<footer id="footer" class="footer text-center">
    <div class="container">
        <!--/* This template is released under the Creative Commons Attribution 3.0 License. Please keep the attribution link below when using for your own project. Thank you for your support. :) If you'd like to use the template without the attribution, you can buy the commercial license via our website: themes.3rdwavemedia.com */-->
        <small class="copyright">Designed with <i class="fas fa-heart"></i> by <a href="https://themes.3rdwavemedia.com/" target="_blank">Xiaoying Riley</a> for developers</small>

    </div><!--//container-->
</footer><!--//footer-->


<!-- Main Javascript -->
<script type="text/javascript" src="{{URL::to('assets_changelog/plugins/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets_changelog/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets_changelog/plugins/prism/prism.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets_changelog/plugins/jquery-scrollTo/jquery.scrollTo.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets_changelog/plugins/lightbox/dist/ekko-lightbox.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets_changelog/plugins/stickyfill/dist/stickyfill.min.js')}}"></script>
<script type="text/javascript" src="{{URL::to('assets_changelog/js/main.js')}}"></script>

</body>
</html>

