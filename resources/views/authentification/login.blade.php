<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- X-CSRF token for authenticating api calls -->
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>{{$title}}</title>
    <meta property="og:site_name" content="{{env('APP_NAME')}}" />
    <meta name="description" content="Web Development Tutorials &amp; Resources" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:image" content="https://tutorialzine.com/images/tutorialzine-share-image.png" />
    <meta property="og:type" content="website" />

    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" sizes="32x32" href="">
    <link rel="icon" type="image/png" sizes="16x16" href="">
    <link rel="shortcut icon" href="">
    <link rel="mask-icon" href="" color="#1da7da">
    <meta name="apple-mobile-web-app-title" content="{{env('APP_NAME')}}">
    <meta name="application-name" content="{{env('APP_NAME')}}">
    <meta name="theme-color" content="#ffffff">
    {{--jquery--}}
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    {{--styles--}}
    <link rel="stylesheet" href="{{URL::to('assets/css/pnotify.custom.min.css')}}">
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Jockey+One,Nunito:300,400,700,900" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="{{URL::to('assets/build/css/main.css')}}" rel="stylesheet">

</head>
<body>
@include('layouts.errors')
<div class="page__wrapper">

    <header class="header--main animate-search">

        <div class="container-fluid">
            <nav class="nav--main">
                <div class="nav__logo">
                    <a href="{{env('APP_URL')}}">
                        Future <span>Laboratory</span>
                    </a>
                </div>

                <ul class="nav__items nav__items--left">
                    <li class="nav__item nav__item--main">
                        <a href="#" class="articles ">
                            Articles
                        </a>
                    </li>
                    <li class="nav__item nav__item--main">
                        <a href="#" class="forum">
                            Forum
                        </a>
                    </li>
                </ul>


            </nav>
            <!-- .nav--main -->
        </div>
        <a href="#0" class="nav-trigger icon-menu"></a>
    </header>
    <div class="cover-layer"></div>
    <!-- cover main content when search form is open -->
    <main class="main-content">
        <div class="content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-centered">


                        <div class="section">
                            <div class="panel-heading">
                                @if(session('email'))
                                    <img class="icon-user icon-form" src="{{get_gravatar(session('email'))}}" style="border:1px solid transparent;border-radius: 100%;">
                                    @else
                                <i class="icon-user icon-form"></i>
                                @endif
                                <h2>Login</h2>
                            </div>

                            <div class="alert" hidden>
                                <div class="loader loader-page" hidden>
                                    <div class="loader-container">
                                        <div class="spinner">
                                            <div class="checkmark draw"></div>
                                            <div class="cross draw"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="message"></div>
                            </div>






                            <div class="panel-body">
                                <!--<div class="divider" data-divider="or"></div>-->
                                <form class="form-horizontal form--flex" role="form" method="POST" action="{{URL::to('signin')}}">
                                    {{csrf_field()}}
                                    @if(!session('email'))
                                    <div class="form-group">
                                        <div class="controls">
                                            <input id="email" type="email" class="form-control" name="email" value="{{session('email') ? session('email') : ''}}" required>
                                            <label for="email" data-content="Email">Email</label>
                                        </div>
                                    </div>
                                    @endif
                                    @if(session('email'))
                                    <div class="form-group">

                                        <div class="controls">
                                            <input id="password" type="password" class="form-control" name="password" required>
                                            <label for="password" data-content="Password">Password</label>
                                        </div>

                                    </div>
                                    @endif
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-warning animated sign-in">
                                <span>
                                    Log in
                                </span>
                                        </button>

                                        <a class="btn-link" href="#">Forgot Your Password?</a>
                                        @if(session('email'))
                                        <a class="btn-link" href="{{URL::to('switch')}}" style="font-size:22px">Switch Account</a>
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>


    <footer>
        <h3>A community that learns together.</h3>

    </footer>

    <div class="modal fade" id="unhandled-error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>

                <div class="modal-body">
                    <h3>Oops! Something went wrong :( Try again...</h3>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-centered" data-dismiss="modal"><span>OK</span></button>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.8.0/highlight.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>


<script src="{{URL::to('assets/build/js/bundle-c099f8dd47.js')}}"></script>
{{--pnotify js--}}

<script src="{{URL::to('assets/js/pnotify.custom.min.js')}}"></script>

</body>

</html>
