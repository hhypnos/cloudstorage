<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- X-CSRF token for authenticating api calls -->
    <meta name="csrf-token" content="{{csrf_token()}}">

    <title>{{$title}}</title>
    <meta property="og:site_name" content="{{env('APP_NAME')}}" />
    <meta name="description" content="Web Development Tutorials &amp; Resources" />
    <meta property="og:title" content="{{$title}}" />
    <meta property="og:url" content="{{url()->current()}}" />
    <meta property="og:image" content="https://tutorialzine.com/images/tutorialzine-share-image.png" />
    <meta property="og:type" content="website" />

    <link rel="apple-touch-icon" sizes="180x180" href="">
    <link rel="icon" type="image/png" sizes="32x32" href="">
    <link rel="icon" type="image/png" sizes="16x16" href="">
    <link rel="shortcut icon" href="">
    <link rel="mask-icon" href="" color="#1da7da">
    <meta name="apple-mobile-web-app-title" content="{{env('APP_NAME')}}">
    <meta name="application-name" content="{{env('APP_NAME')}}">
    <meta name="theme-color" content="#ffffff">
    <!-- Include our stylesheet -->
    <link href="{{URL::to('assets/css/styles.css')}}" rel="stylesheet"/>

</head>
<body>

<div class="filemanager">

    <div class="search">
        <input type="search" placeholder="Find a file.." />
    </div>

    <div class="breadcrumbs"></div>

    <ul class="data"></ul>

    <div class="nothingfound">
        <div class="nofiles"></div>
        <span>No files here.</span>
    </div>

</div>

<!-- Include our script files -->
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="{{URL::to('assets/js/script.js')}}"></script>

</body>
</html>