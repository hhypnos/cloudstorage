<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('changelogs',function(){
   return view('changelog')->with(['title'=>'ChangeLogs']);
});
Route::group(['middleware' => ['guest']], function () {

    Route::get('switch', 'AccountController@switchAccount');
    Route::get('login', 'AccountController@login')->name('login');
    Route::post('signin', 'AccountController@signin');

});

//authentificated

//validate files on authentification
Route::any('validate',function(){
    if(\Auth::check()){
        return response()->json(['success' => 'success'], 200);
    }
    return abort(401);
});
//end validate files on authentification
Route::group(['middleware' => ['auth']], function () {
    Route::get('cloud', 'AccountController@cloud');
    Route::post('scanner', 'AccountController@scanner');//ajax to scan folder
    //logout
    Route::get('logout', 'AccountController@logout')->name('logout');

});
